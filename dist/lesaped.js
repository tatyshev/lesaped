/*!
 * Lesaped.js v0.0.0
 * (c) 2017 Ruslan Tatyshev
 * Released under the MIT License.
 */
!function (window, document) {
'use strict';

var isArray = Array.isArray;

var isDev = (function () {
  return 'sample' === (function sample () {}).name;
})();

function arrayify (object) {
  object = isArray(object) ? object : object != null ? [object] : [];
  return slice(object);
}

function stringify (object) {
  return Object.prototype.toString.call(object);
}

function isFunction (object) {
  return typeof object === 'function';
}

function isPlainObject (value) {
  if (typeof value !== 'object' || stringify(value) !== '[object Object]') {
    return false;
  }

  var proto = Object.getPrototypeOf(value);

  if (proto === null) return true;

  var Ctor = proto.constructor;

  return isFunction(Ctor) && Ctor instanceof Ctor;
}

function hasOwn (object, key) {
  return Object.prototype.hasOwnProperty.call(object, key);
}

function slice (list, start, end) {
  if (list == null) return [];
  return Array.prototype.slice.call(list, start || 0, end);
}

function splice (list, start, end) {
  if (list == null) return [];
  return Array.prototype.splice.call(list, start || 0, end);
}

function classOf (object) {
  if (object == null) {
    return;
  }

  if (object.__proto__ != null) {
    return object.__proto__.constructor;
  } else {
    return object.constructor;
  }
}

function each (object, callback) {
  if (isArray(object)) {
    var index = -1;
    var length = object.length;
    var key = null;

    while (++index < length) {
      if (callback(object[index], index) === false) {
        break;
      }
    }
  } else {
    for(key in object) {
      if (hasOwn(object, key)) {
        if (callback(object[key], key) === false) {
          break;
        }
      }
    }
  }

  return object;
}

function map (object, callback) {
  var result = isArray(object) ? [] : {};

  each(object, function (value, key) {
    result[key] = callback(value, key);
  });

  return result;
}

function assign (target, source) {
  for (var key in source) {
    if (hasOwn(source, key)) {
      target[key] = source[key];
    }
  }

  return target;
}

function extend (parent, proto) {
  var child = function () { parent.apply(this, arguments); };

  child.$super = parent;

  child.prototype = Object.create(parent.prototype, {
    constructor: {
      value: child,
      enumerable: false
    }
  });

  assign(child.prototype, proto);
  assign(child, parent);

  return child;
}

function $ (query, context) {
  context = context || document;
  query = query === window ? document : query;

  if (typeof query === 'string') {
    return slice(context.querySelectorAll(query));
  } else {
    if (query === document || query instanceof HTMLElement) {
      return [query];
    } else {
      return [];
    }
  }
}

function $$ (query, context) {
  var list = $(query, context);
  if (list) return list[0];
}

var random36 = (function () {
  var cid = 0;
  return function () {
    cid++;
    return Math.random().toString(36).slice(2) + cid++;
  };
})();


var utils = Object.freeze({
	isArray: isArray,
	isDev: isDev,
	arrayify: arrayify,
	stringify: stringify,
	isFunction: isFunction,
	isPlainObject: isPlainObject,
	hasOwn: hasOwn,
	slice: slice,
	splice: splice,
	classOf: classOf,
	each: each,
	map: map,
	assign: assign,
	extend: extend,
	$: $,
	$$: $$,
	random36: random36
});

function Lesaped (configs) {
  var self = this;

  self.super = classOf(this);
  self.configs = {};

  if (configs) {
    if (isPlainObject(configs)) {
      self.configs = configs;
    } else {
      throw new Error('Lesaped configs should be a plain object.');
    }
  }

  each(self.super.plugins, function (plugin) {
    if (isFunction(plugin.include)) {
      plugin.include.apply(self);
    }
  });
}

Lesaped.use = function (plugin) {
  if (typeof plugin.name !== 'string') {
    throw new Error('Plugin should have a "name" property.');
  }

  var Super = this;
  var plugins = Super.plugins = Super.plugins || {};
  var args = slice(arguments, 1);

  if (plugins[plugin.name]) {
    return console.warn(plugin.name + ' already used.');
  }

  args.unshift(Super);

  if (isFunction(plugin.install)) {
    plugin.install.apply(plugin, args);
  }

  plugins[plugin.name] = plugin;

  return Super;
};

Lesaped.extend = function () {
  var child = extend(this);
  child.plugins = assign({}, child.plugins);
  return child;
};

var initializr = {
  name: 'lesaped/initializr',

  include: function () {
    var configs = this.configs;

    if (configs.scope == null) {
      this.scope = document;
      return;
    }

    this.scope = $$(configs.scope);

    if (this.scope == null) {
      throw new Error('Can\'t resolve scope given as "' + configs.scope + '"');
    }
  }
};

var utils$1 = {
  name: 'lesaped/utils',

  install: function (Lesaped) {
    Lesaped.utils = utils;
  }
};

var events = {
  name: 'lesaped/events',

  include: function () {
    this.$events = {};
  },

  install: function (Lesaped) {
    var proto = Lesaped.prototype;

    proto.on = function (key, context, listener) {
      if (listener == null && isFunction(context)) {
        listener = context;
        context = null;
      }

      var events = this.$events;
      var listeners = events[key] = events[key] || [];

      listeners.push([listener, context]);
      return listener;
    };

    proto.off = function (key, listener) {
      var listeners = this.$events[key] || [];
      var i = listeners.length;

      while (i--) {
        listener === listeners[i][0] && splice(listeners, i, 1);
      }
    };

    proto.emit = function () {
      var args = slice(arguments, 0);
      var key = args.shift();
      var listeners = this.$events[key];

      each(listeners, function (listener) {
        listener[0].apply(listener[1], args);
      });
    };
  }
};

function Context (application) {
  this.application = application;
}

function Component () {
}

Component.create = function (props, context) {
  this.props = props;
  this.context = context;
  return this;
};

function Module (component, context) {
  this.component = component;
  this.context = context;
  this.heap = {};
}

Module.prototype.init = function () {
};

var modules = {
  name: 'lesaped/modules',

  include: function () {
    this.modules = {};
  },

  install: function (Lesaped) {
    Lesaped.prototype.module = function (name, builder) {
      var modules = this.modules;

      if (modules[name]) {
        throw new Error('module "' + name + '" already defined');
      }

      if (!isFunction(builder)) {
        throw new Error('builder for module "' + name + '" not given.');
      }

      var context = new Context(this);
      var props = builder(context);
      var component = Component.create(props, context);

      modules[name] = new Module(component, context);
    };

    Lesaped.prototype.init = function () {
      each(this.modules, function (value) {
        value.init();
      });
    };
  }
};

Lesaped.use(initializr);
Lesaped.use(utils$1);
Lesaped.use(events);
Lesaped.use(modules);

/* eslint-disable no-undef */
if (typeof exports === 'object' && typeof module !== 'undefined') {
  module.exports = Lesaped;
} else {
  if (typeof define === 'function' && define.amd) {
     define('Lesaped', Lesaped);
  } else {
    if (typeof window !== 'undefined' && !window.Lesaped) {
      window.Lesaped = Lesaped;
    } else {
      console.warn('Lesaped already required.');
    }
  }
}
/* eslint-enable no-undef */

}(window, document);