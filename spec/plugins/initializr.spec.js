import Lesaped from 'src/index';

describe('plugins.initializr', function () {
  it('should initialize scope with document when option not given.', function () {
    var app = new Lesaped();
    expect(app.scope).toBe(document);
  });

  it('should initialize scope when selector given.', function () {
    var app = new Lesaped({ scope: '#app' });
    var app_el = document.getElementById('app');

    expect(app.scope).toBe(app_el);
  });

  it('should initialize scope when DOMElement given.', function () {
    var app_el = document.getElementById('app');
    var app = new Lesaped({ scope: app_el });
    expect(app.scope).toBe(app_el);
  });

  it('should throw error when scope cant\'t be resolved.', function () {
    var app = () => new Lesaped({ scope: '#not_existing_element' });
    expect(app).toThrow();
  });
});
