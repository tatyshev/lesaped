import Lesaped from 'src/index';
import { createContext } from 'spec/helpers';

describe('plugins.modules.init', function () {
  var app, context;

  beforeEach(function () {
    context = createContext();
    app = new Lesaped({ scope: context });

    app.module('one', function () {
      return {};
    });

    spyOn(app.modules.one, 'init');

    app.init();
  });

  it('module init method should have been called.', function () {
    expect(app.modules.one.init).toHaveBeenCalled();
  });

  it('module builder should be a function.', function () {
    expect(() => app.module('ololo', 123)).toThrowError();
  });

  it('module can\'t be defined without builder.', function () {
    expect(() => app.module('ololo')).toThrowError();
  });

  it('module can\'t defined twice.', function () {
    expect(() => app.module('one', function () {})).toThrowError();
  });
});
