import Lesaped from 'src/index';

describe('plugins.modules', function () {
  it('should define new module.', function () {
    var app = new Lesaped();
    var builder = jasmine.createSpy('builder').and.callFake(function () {
      return { one: 1, two: 2 };
    });

    app.module('one', builder);

    expect(builder).toHaveBeenCalled();
    expect(app.modules.one.component.props).toEqual({one: 1, two: 2});
  });
});
