import Lesaped from 'src/index';

describe('plugins.utils', function () {
  it('should be defined.', function () {
    expect(Lesaped.utils).toBeDefined();
  });
});
