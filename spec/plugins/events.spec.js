import Lesaped from 'src/index';

describe('plugins.events', function () {
  var lesaped = null;

  beforeEach(function () {
    lesaped = new Lesaped();
  });

  describe('events.on', function () {
    it('should add listeners to events list.', function () {
      var handler = function () {};
      var returned = lesaped.on('init', handler);
      expect(returned).toBe(handler);
      expect(lesaped.$events.init.length).toBe(1);
      lesaped.on('init', handler);
      expect(lesaped.$events.init.length).toBe(2);
    });
  });

  describe('events.emit', function () {
    it('should call event listeners.', function () {
      var handler = jasmine.createSpy('handler');
      lesaped.on('init', handler);
      lesaped.emit('init');
      lesaped.emit('init');
      expect(handler).toHaveBeenCalledTimes(2);
    });
  });

  describe('events.emit with params', function () {
    it('should call event listeners.', function () {
      var spy = jasmine.createSpy('handler');
      var handler = function (one, two) {
        spy();
        expect(one).toEqual(1);
        expect(two).toEqual(2);
      };

      lesaped.on('init', handler);
      lesaped.emit('init', 1, 2);

      expect(spy).toHaveBeenCalled();
    });
  });

  describe('events.off', function () {
    it('should remove give listener.', function () {
      var handler = function () {};
      lesaped.on('init', handler);
      lesaped.on('init', handler);
      lesaped.off('init', handler);
      expect(lesaped.$events.init.length).toBe(0);
    });
  });

  describe('events.on with context', function () {
    it('should add listeners and context to events list.', function () {
      var spy = jasmine.createSpy('spy');

      var handler = function () {
        expect(this).toEqual('foo!');
        spy();
      };

      var context = 'foo!';

      lesaped.on('init', context, handler);
      lesaped.emit('init');

      expect(spy).toHaveBeenCalled();
    });
  });
});
