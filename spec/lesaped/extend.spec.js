import Lesaped from 'src/lesaped';

describe('Lesaped.extend [js]', function () {
  it('should clone object.', function () {
    var Child = Lesaped.extend();
    expect(typeof Child.use).toBe('function');
    expect(typeof Child.extend).toBe('function');
  });
});
