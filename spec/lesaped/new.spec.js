import Lesaped from 'src/lesaped';

describe('Lesaped.new', function () {
  it('should throw error when wront config format given.', function () {
    expect(() => new Lesaped(123)).toThrow();
    expect(() => new Lesaped('sadas')).toThrow();
    expect(() => new Lesaped(null)).not.toThrow();
    expect(() => new Lesaped(false)).not.toThrow();
    expect(() => new Lesaped({})).not.toThrow();
  });
});
