import Lesaped from 'src/lesaped';

describe('Lesaped.use', function () {
  var lesaped;

  beforeEach(function () {
    lesaped = Lesaped.extend();
  });

  it('should have unique "plugins" object for each class.', function () {
    var plugin = { name: 'ololo' };

    var one = Lesaped.extend();
    var two = Lesaped.extend();

    one.use(plugin);

    expect('ololo' in one.plugins).toBe(true);
    expect('ololo' in two.plugins).toBe(false);
  });

  it('should install plugin when plugin given as "object".', function () {
    var included = jasmine.createSpy('include');

    var plugin = {
      name: 'foobar',

      install: function (self, one, two) {
        self.prototype.foo = function () {
          return 'bar';
        };

        expect(self).toBe(lesaped);
        expect(one).toBe('one');
        expect(two).toBe('two');
        expect(this.method()).toBe('trololo');
      },

      include: function () {
        included();
        expect(this.configs).toEqual({});
        expect(this.foo()).toBe('bar');
      },

      method: function () {
        return 'trololo';
      }
    };

    lesaped.use(plugin, 'one', 'two');
    new lesaped();

    expect(included).toHaveBeenCalled();
  });
});
