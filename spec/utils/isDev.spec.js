import dev from 'dist/lesaped';
import prod from 'dist/lesaped.min';

describe('utils.isDev', function () {
  it('should be development.', function () {
    expect(dev.utils.isDev).toBe(true);
    expect(prod.utils.isDev).toBe(false);
  });
});
