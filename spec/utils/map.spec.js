import { map } from 'src/utils';

describe('utils.map', function () {
  it('should return changed array when array given.', function () {
    var result = map([1,2,3], function (value) {
      return value * 2;
    });

    expect(result).toEqual([2,4,6]);
  });

  it('should return changed object when object given.', function () {
    var result = map({ one: 1, two: 2, three: 3 }, function (value) {
      return value * 2;
    });

    expect(result).toEqual({ one: 2, two: 4, three: 6 });
  });
});
