import { random36 } from 'src/utils';

describe('utils.random36', function () {
  it('should generate unique strings.', function () {
    var list = [];
    for (var i = 0; i < 100; i++) list.push(random36());
    expect(jQuery.unique(list).length).toEqual(100);
  });
});
