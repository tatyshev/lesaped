import { splice } from 'src/utils';

describe('utils.splice', function () {
  it('should return subset of list.', function () {
    expect(splice([1,2,3,4], 1, 2)).toEqual([2, 3]);
    expect(splice([1,2,3,4], 0, 2)).toEqual([1, 2]);
  });

  it('should return empty list when null given.', function () {
    expect(splice(null, 1, 3)).toEqual([]);
    expect(splice(undefined, 1, 3)).toEqual([]);
  });
});
