import { classOf } from 'src/utils';

describe('utils.classOf', function () {
  it('should return class of function.', function () {
    var fn = function () {
      expect(classOf(this)).toBe(fn);
    };

    new fn();
  });
});
