import { assign } from 'src/utils';

describe('utils.assign', function () {
  it('should copy properties to children when source given.', function () {
    var target = { one: 1 };
    var source = { two: 2, three: 3 };

    assign(target, source);

    expect(target).toEqual({ one: 1, two: 2, three: 3 });
  });
});
