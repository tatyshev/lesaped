import { $, $$ } from 'src/utils';
import { createContext } from 'spec/helpers';

describe('utils.$', function () {
  it('should return list of elements when context and selector given.', function () {
    var context = createContext('<div class="one"></div><span class="two"></span>');
    var list = $('.one, .two', context).map(function (el) { return el.toString(); });
    expect(list).toEqual(['[object HTMLDivElement]', '[object HTMLSpanElement]']);
  });

  it('should return list of elements when selector only given.', function () {
    var list = $('body');
    expect(list).toEqual([document.body]);
  });

  it('should return list of elements when element only given.', function () {
    expect($(document.body)).toEqual([document.body]);
    expect($(document)).toEqual([document]);
    expect($(window)).toEqual([document]);
    expect($(null)).toEqual([]);
  });
});

describe('utils.$$', function () {
  it('should return element when context and selector given.', function () {
    var context = createContext('<div class="one"></div><span class="two"></span>');
    var element = $$('.one, .two', context).toString();
    expect(element).toBe('[object HTMLDivElement]');
  });

  it('should return element when selector only given.', function () {
    var element = $$('body');
    expect(element).toBe(document.body);
  });

  it('should return element when element only given.', function () {
    expect($$(document.body)).toBe(document.body);
    expect($$(document)).toBe(document);
    expect($$(window)).toBe(document);
    expect($$(null)).toBeUndefined();
  });
});
