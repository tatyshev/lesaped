import { isPlainObject } from 'src/utils';

describe('utils', function () {
  describe('isPlainObject', function () {
    it('should return `true` when argument is a plain object.', function () {
      expect( isPlainObject({}) ).toBeTruthy();
      expect( isPlainObject(Object.create({})) ).toBeTruthy();
      expect( isPlainObject(Object.create(null)) ).toBeTruthy();
      expect( isPlainObject(Object.create(Object.prototype)) ).toBeTruthy();
      expect( isPlainObject({ foo: 'bar' }) ).toBeTruthy();
    });

    it('should return `false` when argument is not a plain object.', function () {
      var foo = function () {};
      expect( isPlainObject(new foo()) ).toBeFalsy();
      expect( isPlainObject(/foo/) ).toBeFalsy();
      expect( isPlainObject(function () {}) ).toBeFalsy();
      expect( isPlainObject(1) ).toBeFalsy();
      expect( isPlainObject(['foo', 'bar']) ).toBeFalsy();
      expect( isPlainObject([]) ).toBeFalsy();
      expect( isPlainObject(null) ).toBeFalsy();
    });
  });
});
