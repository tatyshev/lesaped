import { slice } from 'src/utils';

describe('utils.slice', function () {
  it('should return subset of list.', function () {
    expect(slice([1,2,3,4], 1, 3)).toEqual([2, 3]);
    expect(slice([1,2,3,4], 0, -2)).toEqual([1, 2]);
  });

  it('should return empty list when null given.', function () {
    expect(slice(null, 1, 3)).toEqual([]);
    expect(slice(undefined, 1, 3)).toEqual([]);
  });

  it('should clone list when start not given.', function () {
    var list = [1,2,3];
    expect(list).not.toBe(slice(list));
  });
});
