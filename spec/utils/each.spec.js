import { each } from 'src/utils';

describe('utils.each', function () {
  it('should iterate over object keys/value pairs.', function () {
    var object = { one: 1, two: 2, three: 3 };
    var result = [];

    var output = each(object, function (value, key) {
      result.push(key);
      result.push(value);
    });

    expect(result).toEqual(['one', 1, 'two', 2, 'three', 3]);
    expect(output).toEqual(object);
  });

  it('should iterate over array values.', function () {
    var array = ['one', 'two', 'three'];
    var result = {};

    var output = each(array, function (value, index) {
      result[value] = index + 1;
    });

    expect(result).toEqual({ one: 1, two: 2, three: 3});
    expect(output).toEqual(array);
  });


  it('should do nothing when null given.', function () {
    expect(each(null)).toBe(null);
  });
});
