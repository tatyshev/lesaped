export function createContext (markup) {
  var body = document.createElement('body');
  body.innerHTML = markup || '';
  return body;
}
