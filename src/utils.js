export var isArray = Array.isArray;

export var isDev = (function () {
  return 'sample' === (function sample () {}).name;
})();

export function arrayify (object) {
  object = isArray(object) ? object : object != null ? [object] : [];
  return slice(object);
}

export function stringify (object) {
  return Object.prototype.toString.call(object);
}

export function isFunction (object) {
  return typeof object === 'function';
}

export function isPlainObject (value) {
  if (typeof value !== 'object' || stringify(value) !== '[object Object]') {
    return false;
  }

  var proto = Object.getPrototypeOf(value);

  if (proto === null) return true;

  var Ctor = proto.constructor;

  return isFunction(Ctor) && Ctor instanceof Ctor;
}

export function hasOwn (object, key) {
  return Object.prototype.hasOwnProperty.call(object, key);
}

export function slice (list, start, end) {
  if (list == null) return [];
  return Array.prototype.slice.call(list, start || 0, end);
}

export function splice (list, start, end) {
  if (list == null) return [];
  return Array.prototype.splice.call(list, start || 0, end);
}

export function classOf (object) {
  if (object == null) {
    return;
  }

  if (object.__proto__ != null) {
    return object.__proto__.constructor;
  } else {
    return object.constructor;
  }
}

export function each (object, callback) {
  if (isArray(object)) {
    var index = -1;
    var length = object.length;
    var key = null;

    while (++index < length) {
      if (callback(object[index], index) === false) {
        break;
      }
    }
  } else {
    for(key in object) {
      if (hasOwn(object, key)) {
        if (callback(object[key], key) === false) {
          break;
        }
      }
    }
  }

  return object;
}

export function map (object, callback) {
  var result = isArray(object) ? [] : {};

  each(object, function (value, key) {
    result[key] = callback(value, key);
  });

  return result;
}

export function assign (target, source) {
  for (var key in source) {
    if (hasOwn(source, key)) {
      target[key] = source[key];
    }
  }

  return target;
}

export function extend (parent, proto) {
  var child = function () { parent.apply(this, arguments); };

  child.$super = parent;

  child.prototype = Object.create(parent.prototype, {
    constructor: {
      value: child,
      enumerable: false
    }
  });

  assign(child.prototype, proto);
  assign(child, parent);

  return child;
}

export function $ (query, context) {
  context = context || document;
  query = query === window ? document : query;

  if (typeof query === 'string') {
    return slice(context.querySelectorAll(query));
  } else {
    if (query === document || query instanceof HTMLElement) {
      return [query];
    } else {
      return [];
    }
  }
}

export function $$ (query, context) {
  var list = $(query, context);
  if (list) return list[0];
}

export var random36 = (function () {
  var cid = 0;
  return function () {
    cid++;
    return Math.random().toString(36).slice(2) + cid++;
  };
})();
