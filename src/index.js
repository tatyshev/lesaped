import Lesaped from './lesaped';

import initializr from './plugins/initializr';
import utils from './plugins/utils';
import events from './plugins/events';
import modules from './plugins/modules/index';

Lesaped.use(initializr);
Lesaped.use(utils);
Lesaped.use(events);
Lesaped.use(modules);

/* eslint-disable no-undef */
if (typeof exports === 'object' && typeof module !== 'undefined') {
  module.exports = Lesaped;
} else {
  if (typeof define === 'function' && define.amd) {
     define('Lesaped', Lesaped);
  } else {
    if (typeof window !== 'undefined' && !window.Lesaped) {
      window.Lesaped = Lesaped;
    } else {
      console.warn('Lesaped already required.');
    }
  }
}
/* eslint-enable no-undef */
