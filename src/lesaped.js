import { each, classOf, isFunction, slice, extend, assign, isPlainObject } from './utils';

export default function Lesaped (configs) {
  var self = this;

  self.super = classOf(this);
  self.configs = {};

  if (configs) {
    if (isPlainObject(configs)) {
      self.configs = configs;
    } else {
      throw new Error('Lesaped configs should be a plain object.');
    }
  }

  each(self.super.plugins, function (plugin) {
    if (isFunction(plugin.include)) {
      plugin.include.apply(self);
    }
  });
}

Lesaped.use = function (plugin) {
  if (typeof plugin.name !== 'string') {
    throw new Error('Plugin should have a "name" property.');
  }

  var Super = this;
  var plugins = Super.plugins = Super.plugins || {};
  var args = slice(arguments, 1);

  if (plugins[plugin.name]) {
    return console.warn(plugin.name + ' already used.');
  }

  args.unshift(Super);

  if (isFunction(plugin.install)) {
    plugin.install.apply(plugin, args);
  }

  plugins[plugin.name] = plugin;

  return Super;
};

Lesaped.extend = function () {
  var child = extend(this);
  child.plugins = assign({}, child.plugins);
  return child;
};
