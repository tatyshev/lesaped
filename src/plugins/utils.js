import * as utils from '../utils';

export default {
  name: 'lesaped/utils',

  install: function (Lesaped) {
    Lesaped.utils = utils;
  }
};
