import { isFunction, splice, slice, each } from '../utils';

export default {
  name: 'lesaped/events',

  include: function () {
    this.$events = {};
  },

  install: function (Lesaped) {
    var proto = Lesaped.prototype;

    proto.on = function (key, context, listener) {
      if (listener == null && isFunction(context)) {
        listener = context;
        context = null;
      }

      var events = this.$events;
      var listeners = events[key] = events[key] || [];

      listeners.push([listener, context]);
      return listener;
    };

    proto.off = function (key, listener) {
      var listeners = this.$events[key] || [];
      var i = listeners.length;

      while (i--) {
        listener === listeners[i][0] && splice(listeners, i, 1);
      }
    };

    proto.emit = function () {
      var args = slice(arguments, 0);
      var key = args.shift();
      var listeners = this.$events[key];

      each(listeners, function (listener) {
        listener[0].apply(listener[1], args);
      });
    };
  }
};
