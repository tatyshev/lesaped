import { each, isFunction } from '../../utils';
import Context from './context';
import Component from './component';
import Module from './module';

export default {
  name: 'lesaped/modules',

  include: function () {
    this.modules = {};
  },

  install: function (Lesaped) {
    Lesaped.prototype.module = function (name, builder) {
      var modules = this.modules;

      if (modules[name]) {
        throw new Error('module "' + name + '" already defined');
      }

      if (!isFunction(builder)) {
        throw new Error('builder for module "' + name + '" not given.');
      }

      var context = new Context(this);
      var props = builder(context);
      var component = Component.create(props, context);

      modules[name] = new Module(component, context);
    };

    Lesaped.prototype.init = function () {
      each(this.modules, function (value) {
        value.init();
      });
    };
  }
};
