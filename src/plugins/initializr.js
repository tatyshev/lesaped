import { $$ } from '../utils';

export default {
  name: 'lesaped/initializr',

  include: function () {
    var configs = this.configs;

    if (configs.scope == null) {
      this.scope = document;
      return;
    }

    this.scope = $$(configs.scope);

    if (this.scope == null) {
      throw new Error('Can\'t resolve scope given as "' + configs.scope + '"');
    }
  }
};
