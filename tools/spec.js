var WebpackDevServer = require('webpack-dev-server');
var webpack = require('webpack');
var resolve = require('path').resolve;
var open = require('open');

var PORT = 7000;
var AGGREGATE_TIMEOUT = 1000;

var compiler = webpack({
  entry: [
    'webpack-dev-server/client?http://localhost:' + PORT + '/',
    resolve('spec/index.js')
  ],

  watchOptions: {
    aggregateTimeout: AGGREGATE_TIMEOUT
  },

  resolve: {
    alias: {
      src: resolve('src'),
      spec: resolve('spec'),
      dist: resolve('dist')
    }
  },

  output: {
    path: resolve('spec'),
    filename: 'bundle.js'
  },

  module: {
    loaders: [
      {
        test: /\.html$/,
        loader: 'raw'
      },

      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel',
        query: {
          plugins: ['transform-es2015-modules-commonjs']
        }
      }
    ]
  }
});

var server = new WebpackDevServer(compiler, {
  contentBase: resolve('spec'),

  watchOptions: {
    aggregateTimeout: AGGREGATE_TIMEOUT
  },

  stats: {
    colors: true
  }
});

server.listen(PORT, '0.0.0.0', function () {
  var runner = 'http://localhost:7000/index.html';
  console.log('Tests runner: ' + runner);
  open(runner);
});
