var closure = require('google-closure-compiler-js').compile;
var rollup = require('rollup').rollup;
var zlib = require('zlib');
var _ = require('./utils');

pack(development);
pack(production);

function pack (callback) {
  var bundle = rollup({ entry: 'src/index.js' });

  bundle.then (function (bundle) {
    var bundled = bundle.generate();
    callback(_.iife(bundled.code));
  });

  bundle.catch(function (reason) {
    console.log(reason);
  });
}

function development (code) {
  _.write('dist/lesaped.js', _.banner + code);
}

function production (code) {
  var minified = closure({
    jsCode: [{ src: code }]
  });

  var output = _.banner + minified.compiledCode;
  var buffer = new Buffer(output, 'utf-8');

  _.write('dist/lesaped.min.js', output);

  zlib.gzip(buffer, function (err, gzipped) {
    _.write('dist/lesaped.min.js.gz', gzipped);
  });
}
